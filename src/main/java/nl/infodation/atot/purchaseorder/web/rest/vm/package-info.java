/**
 * View Models used by Spring MVC REST controllers.
 */
package nl.infodation.atot.purchaseorder.web.rest.vm;
